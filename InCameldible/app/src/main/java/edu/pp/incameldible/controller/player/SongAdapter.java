package edu.pp.incameldible.controller.player;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import java.util.ArrayList;
import android.content.Context;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import edu.pp.incameldible.R;
import edu.pp.incameldible.model.Player.Song;

/**
 * Created by João on 12/09/2017.
 */

public class SongAdapter extends BaseAdapter {


    private ArrayList<Song> songs;
    private LayoutInflater songInf;

    public SongAdapter(Context c, ArrayList<Song> songs){
        this.songs=songs;
        this.songInf=LayoutInflater.from(c);
    }

    @Override
    public int getCount() {
        return songs.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        //map até o songLayout
        LinearLayout songLay = (LinearLayout)songInf.inflate
                (R.layout.song, parent, false);

        //get views de title e artist
        TextView songView = songLay.findViewById(R.id.song_title);
        TextView artistView = songLay.findViewById(R.id.song_artist);

        //get song pela posição
        Song currSong = songs.get(position);

        //get strings de title e artist
        songView.setText(currSong.getTitle());
        artistView.setText(currSong.getArtist());

        //set position comno tag
        songLay.setTag(position);
        return songLay;
    }
}
