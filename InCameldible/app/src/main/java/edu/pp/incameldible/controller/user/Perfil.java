package edu.pp.incameldible.controller.user;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import edu.pp.incameldible.R;

public class Perfil extends AppCompatActivity {

    private Button btnLogout;
    private TextView txName;

    private FirebaseAuth auth;
    private FirebaseUser user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil);

        inicializarComponentes();
        clickButton();
    }

    private void clickButton() {
        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signOut();
                Intent intent = new Intent(Perfil.this, Login.class);
                startActivity(intent);
                finish();
            }
        });
    }

    private void signOut() {
        auth.signOut();
        alert("Desconectado");
    }

    private void inicializarComponentes() {
        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();
        txName = findViewById(R.id.txName);
        btnLogout = findViewById(R.id.btnLogout);

        txName.setText("Olá, "+user.getEmail());

    }



    private void alert(String s) {
        Toast.makeText(Perfil.this, s, Toast.LENGTH_SHORT).show();
    }

}
