package edu.pp.incameldible.controller.user;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.w3c.dom.Text;

import edu.pp.incameldible.R;

public class PerfilGoogle extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener{

    private ImageView ivFoto;
    private TextView tvEmail, tvId;
    private Button btnSignOut;
    private FirebaseAuth mFirebaseAuth;
    private FirebaseAuth.AuthStateListener mAuthStateListener;
    private FirebaseUser mFirebaseUser;
    private GoogleApiClient mGoogleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil_google);

        inicializarComponentes();
        inicializarFirebase();
        conectarGoogleApi();
        clickButton();
    }

    private void clickButton() {
        btnSignOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signOut();
            }
        });
    }

    //TODO desconectar também ta classe do google.
    private void signOut() {
        mFirebaseAuth.signOut();

        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(@NonNull Status status) {
                alert("Desconectado.");
                finish();
            }
        });
    }

    private void conectarGoogleApi() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this,this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    private void inicializarFirebase() {
        mFirebaseAuth = FirebaseAuth.getInstance();

        mAuthStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                mFirebaseUser = firebaseAuth.getCurrentUser();

                if(mFirebaseUser != null){
                    exibirDados(mFirebaseUser);
                }
                else{
                    finish();
                }
            }
        };
    }

    private void exibirDados(FirebaseUser mFirebaseUser) {
        tvEmail.setText(mFirebaseUser.getEmail());
        tvId.setText(mFirebaseUser.getUid());

        Glide.with(PerfilGoogle.this).load(mFirebaseUser.getPhotoUrl()).into(ivFoto);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mFirebaseAuth.addAuthStateListener(mAuthStateListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mFirebaseAuth.removeAuthStateListener(mAuthStateListener);
    }

    private void inicializarComponentes() {
        ivFoto = findViewById(R.id.ivFoto);
        tvEmail = findViewById(R.id.tvEmail);
        tvId = findViewById(R.id.tvId);
        btnSignOut = findViewById(R.id.btnSignOut);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        alert("Falha na conexão");
    }

    private void alert(String s) {
        Toast.makeText(PerfilGoogle.this, s, Toast.LENGTH_SHORT).show();
    }
}
