package edu.pp.incameldible.controller.user;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

import edu.pp.incameldible.R;

public class ResetActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private AutoCompleteTextView email;
    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset);

        inicializarComponentes();
    }

    private void inicializarComponentes() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("reset");
        email = findViewById(R.id.email);
        firebaseAuth = FirebaseAuth.getInstance();
    }

    public void reset (View view){
        firebaseAuth
            .sendPasswordResetEmail(email.getText().toString())
            .addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if(task.isSuccessful()){
                        alert("E-mail enviado.");
                    }
                    else{
                        alert("Falha. Tente novamente.");
                    }
                }
            });
    }

    private void alert(String msg){Toast.makeText(ResetActivity.this, msg, Toast.LENGTH_SHORT).show();}
}
