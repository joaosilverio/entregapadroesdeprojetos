package edu.pp.incameldible.controller.user;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GoogleAuthProvider;

import edu.pp.incameldible.R;
import edu.pp.incameldible.model.firebase.Connection;

public class Login extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener{

    private TextView txEmail;
    private TextView txPwd;
    private FirebaseAuth auth;
    private Button btnLogin;
    private Button btnRegister;
    private LoginButton btnFace;
    private CallbackManager mCallBackManager;
    private SignInButton btnGoogle;
    private FirebaseAuth mFirebaseAuth;
    private GoogleApiClient mGoogleApiClient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        inicializarComponentes();
        inicializarFirebaseCallBack();
        inicializarFirebase();
        conectarGoogleApi();
        eventoClicks();
    }

    private void conectarGoogleApi() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this,this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    private void inicializarFirebase() {
        mFirebaseAuth = FirebaseAuth.getInstance();
    }

    private void inicializarFirebaseCallBack() {
        auth = FirebaseAuth.getInstance();
        mCallBackManager = CallbackManager.Factory.create();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallBackManager.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 1){
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if(result.isSuccess()){
                GoogleSignInAccount account = result.getSignInAccount();
                firebaseLoginG(account);
            }
        }
    }


    private void inicializarComponentes(){
        txEmail = findViewById(R.id.txEmail);
        txPwd = findViewById(R.id.txSenha);
        btnLogin = findViewById(R.id.btnRRegister);
        btnRegister = findViewById(R.id.btnLLogin);
        btnFace = findViewById(R.id.btnFace);
        btnGoogle = findViewById(R.id.btnGoogle);
    }



    private void eventoClicks() {
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), Cadastro.class);
                startActivity(intent);
            }
        });//FIXME novo cadastro com login/senha

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                auth = Connection.getFirebaseAuth();
                String email = txEmail.getText().toString().trim();
                String pwd = txPwd.getText().toString().trim();

                auth.signInWithEmailAndPassword(email, pwd)
                .addOnCompleteListener(Login.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            Intent intent = new Intent(Login.this, Perfil.class);
                            startActivity(intent);
                            finish();
                        }
                        else{
                            alert("Erro (nome de usuário ou senha inválidos.");
                        }
                    }
                });
            }
        });

        btnGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signIn();
            }
        });

        btnFace.registerCallback(mCallBackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                firebaseLogin(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                alert("Operação cancelada!");
            }

            @Override
            public void onError(FacebookException error) {
                alert("Erro: "+error.toString());
            }
        });
    }

    private void signIn(){
        Intent intent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(intent, 1);
    }

    private void firebaseLogin(AccessToken accessToken) {
        AuthCredential credential = FacebookAuthProvider.getCredential(accessToken.getToken());

        auth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            alert("Ok!");
                            Intent i = new Intent(Login.this, Perfil.class);
                            startActivity(i);
                            finish();
                        }else{
                            alert("Erro de autenticação");
                        }

                    }
                });

    }

    private void firebaseLoginG(GoogleSignInAccount account) {
        AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
        mFirebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            Intent i = new Intent(Login.this, PerfilGoogle.class);
                            startActivity(i);
                            finish();
                        }else{
                            alert("Falha na autenticação");
                        }
                    }
                });
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        alert("Falha na conexão");
    }

    public void callReset(View view){
        Intent intent = new Intent(this, ResetActivity.class);
        startActivity(intent);
    }

    private void alert(String msg){
        Toast.makeText(Login.this, msg, Toast.LENGTH_SHORT).show();
    }
}
