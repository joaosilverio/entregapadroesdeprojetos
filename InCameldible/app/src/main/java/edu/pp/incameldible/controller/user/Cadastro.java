package edu.pp.incameldible.controller.user;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import edu.pp.incameldible.R;
import edu.pp.incameldible.model.firebase.Connection;

public class Cadastro extends AppCompatActivity {
//TODO arrumar nome das variavel
    private Button btnRegister;
    private Button btnVoltar;
    private TextView txEmail;
    private TextView txSenha;

    private FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);
        inicializarComponentes();
        eventoClicks();
    }

    private void eventoClicks() {
        btnVoltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String sEmail = txEmail.getText().toString().trim();
                String sSenha = txSenha.getText().toString().trim();
                createUser(sEmail, sSenha);
            }
        });
    }

    private void createUser(String sEmail, String sSenha) {
        auth.createUserWithEmailAndPassword(sEmail, sSenha)
                .addOnCompleteListener(Cadastro.this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    alert("Sucesso!");
                    Intent intent = new Intent(Cadastro.this, Perfil.class);
                    startActivity(intent);
                    finish();
                }else{
                    alert("Erro.");
                }
            }
        });
    }

    private void alert(String msg){
        Toast.makeText(Cadastro.this, msg, Toast.LENGTH_SHORT).show();
    }
    private void inicializarComponentes(){
        txEmail = (TextView) findViewById(R.id.txEmail);
        txSenha = (TextView) findViewById(R.id.txSenha);

        btnVoltar = (Button) findViewById(R.id.btnVoltar);
        btnRegister = (Button) findViewById(R.id.btnRegister);
    }

    @Override
    protected void onStart() {
        super.onStart();
        auth = Connection.getFirebaseAuth();
    }
}
