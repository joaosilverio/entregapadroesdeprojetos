package edu.pp.incameldible.controller.home;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import edu.pp.incameldible.R;
import edu.pp.incameldible.controller.player.PlayerMainActivity;
import edu.pp.incameldible.controller.user.Login;
import edu.pp.incameldible.model.User.User;

public class MainActivity extends AppCompatActivity {

    private EditText edName;
    private EditText edEmail;
    private EditText edLogin;
    private EditText edPwd;
    private ListView listV_dados;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;

    private List<User> allUsers = new ArrayList<>();
    private ArrayAdapter<User> adapterUser;

    private User selectedUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edName = findViewById(R.id.editName);
        edEmail = findViewById(R.id.txEmail);
        edLogin = findViewById(R.id.txLogin);
        edPwd = findViewById(R.id.editSenha);
        listV_dados = findViewById(R.id.listV_dados);

        inicializarFirebase();
        eventoDatabase();

        listV_dados.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView parent, View view, int position, long Id){
                selectedUser = (User)parent.getItemAtPosition(position);
                edName.setText(selectedUser.getName());
                edEmail.setText(selectedUser.getEmail());
                edLogin.setText(selectedUser.getLogin());
                edPwd.setText(selectedUser.getPwd());
            }
        });


    }


    private void eventoDatabase() {
        databaseReference.child("User").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                allUsers.clear();
                for(DataSnapshot objSnapshot:dataSnapshot.getChildren()){
                    User u = objSnapshot.getValue(User.class);
                    allUsers.add(u);
                }
                adapterUser = new ArrayAdapter<User>(MainActivity.this, android.R.layout.simple_list_item_1, allUsers);
                listV_dados.setAdapter(adapterUser);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
    private void inicializarFirebase(){
        FirebaseApp.initializeApp(MainActivity.this);
        firebaseDatabase = FirebaseDatabase.getInstance();
        firebaseDatabase.setPersistenceEnabled(true);//permite alterar ou salvar sem conexão com a web
        databaseReference = firebaseDatabase.getReference();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.menu_new){

            User u = new User();
            u.setUid(UUID.randomUUID().toString());
            u.setName(edName.getText().toString());
            u.setEmail(edEmail.getText().toString());
            u.setLogin(edLogin.getText().toString());
            u.setPwd(edPwd.getText().toString());

            databaseReference.child("User").child(u.getUid()).setValue(u);

            limparCampos();

        }else if(id == R.id.menu_update){

            User u = new User();
            u.setUid(selectedUser.getUid());
            u.setName(edName.getText().toString().trim());
            u.setEmail(edEmail.getText().toString().trim());
            u.setLogin(edLogin.getText().toString().trim());
            u.setPwd(edPwd.getText().toString().trim());
            databaseReference.child("User").child(u.getUid()).setValue(u);
            limparCampos();
        }else if(id == R.id.menu_delete){

            User u = new User();

            u.setUid(selectedUser.getUid());
            databaseReference.child("User").child(u.getUid()).removeValue();
            limparCampos();
        }


        return true;
    }

    private void limparCampos() {
        edName.setText("");
        edEmail.setText("");
        edLogin.setText("");
        edPwd.setText("");
    }

    public void callPlayer(MenuItem item){

        Intent intent = new Intent(this, PlayerMainActivity.class);
        startActivity(intent);

    }

    public void callUser(MenuItem item){
        Intent intent = new Intent(this, Login.class);
        startActivity(intent);
    }

}
